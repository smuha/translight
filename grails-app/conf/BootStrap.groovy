import org.translight.Language
import org.translight.Role
import org.translight.TranslateEngine
import org.translight.User
import org.translight.UserRole

class BootStrap {

    def init = { servletContext ->

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
        def userNotConfirmedRole = new Role(authority: 'ROLE_USER_NOT_CONFIRMED').save(flush: true)

        def user1 = new User(username: 'user', password: 'user')
        user1.save(flush: true)

        def admin1 = new User(username: 'admin', password: 'admin')
        admin1.save(flush: true)

        UserRole.create user1, userRole, true
        UserRole.create admin1, adminRole, true

        assert User.count() == 2
        assert Role.count() == 3
        assert UserRole.count() == 2

        TranslateEngine.create "Yandex", true
        TranslateEngine.create "Bing", true
        TranslateEngine.create "Mymemory", true

        Language.create "English", "EN"
        Language.create "Ukrainian", "UK"
        Language.create "Russian", "RU"
        Language.create "French", "FR"
    }

    def destroy = {
    }
}
