<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div style="width: 300px; margin: 0 auto; text-align: center;">
    <h2>Send message</h2>
    <form action="/sendEmail/send">
        <table>
            <tr>
                <td>Email</td>
                <td>
                    <input name="email" type="email">
                </td>
            </tr>
            <tr>
                <td>Title</td>
                <td>
                    <input name="title" type="text">
                </td>
            </tr>
            <tr>
                <td>Text</td>
                <td>
                    <textarea name="message" style="width: 300px; height: 50px;" ></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button>Send message</button>
                </td>
            </tr>
        </table>
    </form>
</div>

</body>
</html>