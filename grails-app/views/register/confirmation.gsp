<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <asset:stylesheet src="application.css"/>
</head>

<body>

<div class="container">

    <div id="confirmationBox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Email confirmation</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px">
                    <a id="signinlink" href="/login">Sign In</a>
                </div>
            </div>
            <div class="panel-body">
                    <g:if test="${error}">
                        <div id="signupalert" class="alert alert-danger">
                            <p>
                                <b>Error: </b>
                                <g:if test="${error  == 'duplicateUsername'}">
                                    <span id="errorMessage">User with such email is exists already. Please enter another email.</span>
                                </g:if>
                            </p>
                        </div>
                    </g:if>

                    <div class="form-group">
                        <div class="text-center">
                            <h3 class="">Dear ${request.firstName}, many thanks for joining us!</h3>
                            <h4>We have successfully sent you a confirmation letter.</h4>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button href="/register/confirmation" id="btn-reSendEmail" type="submit" class="btn btn-primary center-block">Re-send email</button>
                        </div>
                    </div>

                    <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="/register" id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Sign Up</a>
                            <button id="btn-fbsignup" type="button" class="btn btn-info"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<asset:javascript src="application.js"/>
</body>
</html>