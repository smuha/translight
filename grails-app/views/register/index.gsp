<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <asset:stylesheet src="application.css"/>
</head>

<body>

<div class="container">

    <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px">
                    <a id="signinlink" href="/login">Sign In</a>
                </div>
            </div>
            <div class="panel-body" >
                <form action="/register/user" id="signupform" class="form-horizontal" role="form" method="post">

                    <g:if test="${error}">
                        <div id="signupalert" class="alert alert-danger">
                            <p>
                                <b>Error: </b>
                                <g:if test="${error  == 'duplicateUsername'}">
                                    <span id="errorMessage">User with such email is exists already. Please enter another email.</span>
                                </g:if>
                            </p>
                        </div>
                    </g:if>

                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input id="email" type="text" class="form-control" name="email" placeholder="Email Address">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="firstName" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <input id="firstName" type="text" class="form-control" name="firstName" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" class="btn btn-success"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                            <span style="margin-left:8px;">or</span>
                        </div>
                    </div>

                    <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <oauth:connect provider="facebook" id="facebook-connect-link" type="button" class="btn btn-primary">
                                Sign Up with Facebook
                            </oauth:connect>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<asset:javascript src="application.js"/>
</body>
</html>