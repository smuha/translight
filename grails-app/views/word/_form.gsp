<%@ page import="org.translight.Word" %>



<div class="fieldcontain ${hasErrors(bean: wordInstance, field: 'originalLanguageId', 'error')} required">
    <label for="originalLanguageId">
        <g:message code="word.originalLanguageId.label" default="Original Language Id"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="originalLanguageId" name="originalLanguageId.id" from="${org.translight.Language.list()}"
              optionKey="id" required="" value="${wordInstance?.originalLanguageId?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: wordInstance, field: 'originalText', 'error')} required">
    <label for="originalText">
        <g:message code="word.originalText.label" default="Original Text"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="originalText" required="" value="${wordInstance?.originalText}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: wordInstance, field: 'translateEngine', 'error')} required">
    <label for="translateEngine">
        <g:message code="word.translateEngine.label" default="Translate Engine"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="translateEngine" name="translateEngine.id" from="${org.translight.TranslateEngine.list()}"
              optionKey="id" required="" value="${wordInstance?.translateEngine?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: wordInstance, field: 'translatedLanguageId', 'error')} required">
    <label for="translatedLanguageId">
        <g:message code="word.translatedLanguageId.label" default="Translated Language Id"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="translatedLanguageId" name="translatedLanguageId.id" from="${org.translight.Language.list()}"
              optionKey="id" required="" value="${wordInstance?.translatedLanguageId?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: wordInstance, field: 'translatedText', 'error')} required">
    <label for="translatedText">
        <g:message code="word.translatedText.label" default="Translated Text"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="translatedText" required="" value="${wordInstance?.translatedText}"/>

</div>

