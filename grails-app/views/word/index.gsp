<%@ page import="org.translight.Word" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'word.label', default: 'Word')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-word" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-word" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="dateCreated"
                              title="${message(code: 'word.dateCreated.label', default: 'Date Created')}"/>

            <th><g:message code="word.originalLanguageId.label" default="Original Language Id"/></th>

            <g:sortableColumn property="originalText"
                              title="${message(code: 'word.originalText.label', default: 'Original Text')}"/>

            <th><g:message code="word.translateEngine.label" default="Translate Engine"/></th>

            <th><g:message code="word.translatedLanguageId.label" default="Translated Language Id"/></th>

            <g:sortableColumn property="translatedText"
                              title="${message(code: 'word.translatedText.label', default: 'Translated Text')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${wordInstanceList}" status="i" var="wordInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${wordInstance.id}">${fieldValue(bean: wordInstance, field: "dateCreated")}</g:link></td>

                <td>${fieldValue(bean: wordInstance, field: "originalLanguageId")}</td>

                <td>${fieldValue(bean: wordInstance, field: "originalText")}</td>

                <td>${fieldValue(bean: wordInstance, field: "translateEngine")}</td>

                <td>${fieldValue(bean: wordInstance, field: "translatedLanguageId")}</td>

                <td>${fieldValue(bean: wordInstance, field: "translatedText")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${wordInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
