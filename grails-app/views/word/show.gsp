<%@ page import="org.translight.Word" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'word.label', default: 'Word')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-word" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-word" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list word">

        <g:if test="${wordInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="word.dateCreated.label"
                                                                               default="Date Created"/></span>

                <span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate
                        date="${wordInstance?.dateCreated}"/></span>

            </li>
        </g:if>

        <g:if test="${wordInstance?.originalLanguageId}">
            <li class="fieldcontain">
                <span id="originalLanguageId-label" class="property-label"><g:message
                        code="word.originalLanguageId.label" default="Original Language Id"/></span>

                <span class="property-value" aria-labelledby="originalLanguageId-label"><g:link controller="language"
                                                                                                action="show"
                                                                                                id="${wordInstance?.originalLanguageId?.id}">${wordInstance?.originalLanguageId?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${wordInstance?.originalText}">
            <li class="fieldcontain">
                <span id="originalText-label" class="property-label"><g:message code="word.originalText.label"
                                                                                default="Original Text"/></span>

                <span class="property-value" aria-labelledby="originalText-label"><g:fieldValue bean="${wordInstance}"
                                                                                                field="originalText"/></span>

            </li>
        </g:if>

        <g:if test="${wordInstance?.translateEngine}">
            <li class="fieldcontain">
                <span id="translateEngine-label" class="property-label"><g:message code="word.translateEngine.label"
                                                                                   default="Translate Engine"/></span>

                <span class="property-value" aria-labelledby="translateEngine-label"><g:link
                        controller="translateEngine" action="show"
                        id="${wordInstance?.translateEngine?.id}">${wordInstance?.translateEngine?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${wordInstance?.translatedLanguageId}">
            <li class="fieldcontain">
                <span id="translatedLanguageId-label" class="property-label"><g:message
                        code="word.translatedLanguageId.label" default="Translated Language Id"/></span>

                <span class="property-value" aria-labelledby="translatedLanguageId-label"><g:link controller="language"
                                                                                                  action="show"
                                                                                                  id="${wordInstance?.translatedLanguageId?.id}">${wordInstance?.translatedLanguageId?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${wordInstance?.translatedText}">
            <li class="fieldcontain">
                <span id="translatedText-label" class="property-label"><g:message code="word.translatedText.label"
                                                                                  default="Translated Text"/></span>

                <span class="property-value" aria-labelledby="translatedText-label"><g:fieldValue bean="${wordInstance}"
                                                                                                  field="translatedText"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: wordInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${wordInstance}"><g:message code="default.button.edit.label"
                                                                                     default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
