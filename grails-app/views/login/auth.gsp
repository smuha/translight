<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <asset:stylesheet src="application.css"/>
</head>

<body>

<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign In</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px">
                    <a href="#"> Forgot password?</a>
                </div>
            </div>

            <div style="padding-top:30px" class="panel-body">
                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="loginform" action="/j_spring_security_check" class="form-horizontal" role="form"
                      method="POST" autocomplete="off">

                    <g:if test="${flash.message}">
                        <div id="signupalert" class="alert alert-danger">
                            <p>
                                <b>Error: </b>
                                <g:if test="${error  == 'wrongData'}">
                                    <span id="errorMessage">Your username or password is wrong! Please, try again.</span>
                                </g:if>
                                <g:else>
                                    <span id="errorMessage">${flash.message}</span>
                                </g:else>
                            </p>
                        </div>
                    </g:if>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="j_username" value="${session.confirmedEmail}"
                               placeholder="username or email">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="j_password"
                               placeholder="password">
                    </div>

                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="login-remember" type="checkbox" name="_spring_security_remember_me"
                                       value="1"> Remember me
                            </label>
                        </div>
                    </div>

                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12 controls">
                            <button type="submit" id="btn-login" class="btn btn-success">Login</button>
                            <oauth:connect provider="facebook" id="facebook-connect-link" type="button" class="btn btn-primary">
                                Login with Facebook
                            </oauth:connect>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                Don't have an account!
                                <a href="/register">
                                    <span style="padding-left: 4px;">Sign Up Here</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<asset:javascript src="application.js"/>
</body>
</html>