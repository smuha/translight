<%@ page contentType="text/html"%>
<div class="container">
    <h3>Dear ${firstName}, many thanks for joining us!</h3>
    <h4>Please, click <a href="http://localhost:8080/register/confirmRegistration?confirmCode=${confirmCode}">here</a>
        if you want to complete the registration.</h4>
    <i>Best regards, TransLight.</i>
</div>
