package com.translight

import grails.plugin.springsecurity.annotation.Secured

class SecureController {

    @Secured(['ROLE_ADMIN'])
    def index() {
        render 'Secure access only. Hello dear admin!'
    }

    @Secured(['ROLE_USER'])
    def userCorner() {
        render 'Hello dear user! My congratulations'
    }
}
