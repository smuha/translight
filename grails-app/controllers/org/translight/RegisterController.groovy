package org.translight

import org.apache.commons.lang.RandomStringUtils

class RegisterController {

    static allowedMethods = [user: ['POST', 'GET'], confirmation: ['POST', 'GET']]

    def index() {}

    def user(String email, String password, String firstName) {
        if (request.method == "GET") {
            forward action: "index"
        } else {
            if (User.findByUsername(email)) {
                request.setAttribute("error", "duplicateUsername")
                forward action: "index"
            } else {
                String confirmCode = RandomStringUtils.randomAlphanumeric(40)
                def user = new User(username: email, password: password, confirmRegisterCode: confirmCode)
                user.save(flush: true)
                UserRole.create user, Role.findByAuthority('ROLE_USER_NOT_CONFIRMED'), true

                sendMail {
                    to email
                    subject "TransLight. Account confirmation."
                    body(view: "/confirmRegistrationLetter",
                            model: [firstName: firstName, confirmCode: confirmCode])
                }

                request.setAttribute('firstName', firstName)
                forward action: "confirmation"
            }
        }
    }

    def confirmRegistration(String confirmCode) {
        User user = User.findByConfirmRegisterCode(confirmCode)

        if (user) {
            UserRole.remove user, Role.findByAuthority("ROLE_USER_NOT_CONFIRMED"), true
            UserRole.create user, Role.findByAuthority("ROLE_USER"), true

            user.setConfirmRegisterCode(null)
            user.save(flush: true)

            session.setAttribute("confirmedEmail", user.getUsername())
        }
        redirect(controller: "login", action: "index")
    }

    def confirmation() {}
}