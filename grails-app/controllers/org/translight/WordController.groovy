package org.translight

class WordController {

    static scaffold = true

    def create() {
        request.setAttribute("languages", Language.getAll())
        request.setAttribute("translateEngines", TranslateEngine.getAll())
    }
}
