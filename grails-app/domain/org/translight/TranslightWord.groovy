package org.translight

class TranslightWord {

    String originalText
    String translatedText
    Language originalLanguageId
    Language translatedLanguageId
    Date dateCreated

    static constraints = {
    }
}
