package org.translight

import sun.awt.TracedEventQueue

class TranslateEngine {

    String alias

    static TranslateEngine create(String alias, boolean flush = false) {
        def instance = new TranslateEngine(alias: alias)
        instance.save(flush: flush, insert: true)
        instance
    }

    static constraints = {
    }
}
