package org.translight

class Language {

    String title
    String alias

    static Language create(String title, String alias, boolean flush = false) {
        def instance = new Language(title: title, alias: alias)
        instance.save(flush: flush, insert: true)
        instance
    }

    static constraints = {
    }
}
