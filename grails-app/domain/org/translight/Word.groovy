package org.translight

class Word {

    static scaffold = true

    String originalText
    String translatedText
    Language originalLanguageId
    Language translatedLanguageId
    TranslateEngine translateEngine
    Date dateCreated

    static constraints = {
    }

}
