var yandexTranslateManager = new YandexTranslateManager();
var bingTranslateManager = new BingTranslateManager();
var mymemoryTranslateManager = new MymemoryTranslateManager();

function bingCallback(response) {
    chrome.extension.getViews()[1].document.getElementById("bingOutputArea").value = response;
    chrome.extension.getViews()[1].setSpinnerForBingOff();
    chrome.extension.getViews()[1].resizeBingTextarea();
}

function yandexCallback(response) {
    chrome.extension.getViews()[1].document.getElementById("yandexOutputArea").value = response.text;
    chrome.extension.getViews()[1].setSpinnerForYandexOff();
    chrome.extension.getViews()[1].resizeYandexTextarea();
}

function mymemoryCallback(response) {
    chrome.extension.getViews()[1].document.getElementById("mymemoryOutputArea").value = response.responseData.translatedText;
    chrome.extension.getViews()[1].setSpinnerForMymemoryOff();
    chrome.extension.getViews()[1].resizeMymemoryTextarea();
}