function YandexTranslateManager() {

    const YANDEX_API_KEY = "trnsl.1.1.20131114T204925Z.a46b7f03ba85f91d.90e5b5856aa5f0460101834227f30adcecc3ca09";

    this.translate = function(text, from, to) {
        var LANGUAGE = from + "-" + to;
        var url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
            + "?key=" + YANDEX_API_KEY
            + "&lang=" + LANGUAGE
            + "&text=" + text
            + "&format=html"
            + "&callback=yandexCallback";

        $.get(url);
    };
}