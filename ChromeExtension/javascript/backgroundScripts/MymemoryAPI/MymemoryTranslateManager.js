function MymemoryTranslateManager() {

    const mymemoryEmail = generateRandomEmail();

    this.translate = function(text, from, to) {
        var LANGUAGE = from + "|" + to;
        var url = "http://api.mymemory.translated.net/get"
            + "?q=" + encodeURIComponent(text)
            + "&langpair=" + LANGUAGE
            + "&de=" + mymemoryEmail;

        $.get(url, function(data) {
            mymemoryCallback(data);
        });
    };

    function generateRandomEmail() {
        return generateRandomString(10)
            + "@"
            + generateRandomString(10)
            + "."
            + generateRandomString(2);
    }

    function generateRandomString(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < length; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}