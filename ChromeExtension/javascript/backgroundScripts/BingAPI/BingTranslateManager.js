function BingTranslateManager() {

    const CLIENT_ID = "515f8de1-5d41-4d90-9770-adfd58de4eec";
    const CLIENT_SECRET = "X1nv/Ty5lviS6zaJTI6vvCSMo8Ehl/2dJ5alQSAwWv4=";
    const ERROR_TOKEN_INVALID = "ArgumentException: The Web Token must have a signature at the end. The incoming token did " +
        "not have a signature at the end of the token.";
    const ERROR_TOKEN_EXPIRED = "ArgumentException: The incoming token has expired. Get a new access token from" +
        " the Authorization Server.";
    var access_token = null;

	this.translate = function(text, from, to) {
        $.get(createURL(text, from, to), function(data) {
            var errorTokenInvalidate = data.substring(1, 136);
            var errorTokenExpired = data.substring(1, 105);
            if ((errorTokenInvalidate == ERROR_TOKEN_INVALID) || (errorTokenExpired == ERROR_TOKEN_EXPIRED)) {
                obtainAccessToken(function () {
                    $.get(createURL(text, from, to));
                });
            }
        });
	};

    function createURL(text, from, to) {
        return url = "http://api.microsofttranslator.com/V2/Ajax.svc/Translate" +
            "?appId=Bearer " + encodeURIComponent(access_token) +
            "&from=" + encodeURIComponent(from) +
            "&to=" + encodeURIComponent(to) +
            "&text=" + encodeURIComponent(text) +
            "&oncomplete=bingCallback";
    }

    function obtainAccessToken(callback){
        $.post("https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/", {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            scope: "http://api.microsofttranslator.com",
            grant_type: "client_credentials"
        }, function(data) {
            access_token = data.access_token;
            if (callback) {
                callback();
            }
        });
    };
}