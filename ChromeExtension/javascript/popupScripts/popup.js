var settingsManager = new UserSettingsPopupManager();
var uIManager = new UIPopupManager();

$(document).ready(function() {
    settingsManager.applyUserSettings();
    uIManager.buildInterface();
    addEventHandlers();
    loadPreviousInputTextarea();
});

function loadPreviousInputTextarea() {
    $("#inputArea").val(localStorage.inputArea);
    $('#inputArea').trigger('autosize.resize');
}

function saveTranslateDirectionToStorage() {
    localStorage.languageFrom = $("#translateDirectionFrom option:selected").val();
    localStorage.languageTo = $("#translateDirectionTo option:selected").val();
}

function exchangeLanguageDirection() {
    var translateDirectionFrom = $("#translateDirectionFrom option:selected").val();
    var translateDirectionTo = $("#translateDirectionTo option:selected").val();

    $("#translateDirectionFrom option:selected").removeAttr('selected');
    $("#translateDirectionTo option:selected").removeAttr('selected');

    $('#translateDirectionFrom option[value="'+ translateDirectionTo + '"]').attr("selected", "true");
    $('#translateDirectionTo option[value="'+ translateDirectionFrom + '"]').attr("selected", "true");

    saveTranslateDirectionToStorage();
}

function translateText() {
    var text = $("#inputArea").val();

    if (text != "") {
        var from = $("#translateDirectionFrom").val();
        var to = $("#translateDirectionTo").val();
        chrome.extension.getBackgroundPage().bingTranslateManager.translate(text, from, to);
        chrome.extension.getBackgroundPage().yandexTranslateManager.translate(text, from, to);
        chrome.extension.getBackgroundPage().mymemoryTranslateManager.translate(text, from, to);

        setSpinnerForBingOn();
        setSpinnerForYandexOn();
        setSpinnerForMymemoryOn();
    }
}

function addEventHandlers() {
    $("#inputArea, #yandexOutputArea, #bingOutputArea, #mymemoryOutputArea").autosize();

    $("#translateButton").click(function() {
        translateText();
    });

    $("#exchangeLanguage").keyup(function(e) {
        if ((e.keyCode == 13 || e.keyCode == 32) && !e.ctrlKey) {
            exchangeLanguageDirection();
        }
    });

    $("#exchangeLanguage").click(function(e) {
        exchangeLanguageDirection();
    });

    $("#inputArea").keyup(function(e){
        if (e.ctrlKey && e.keyCode == 13){
            translateText();
        } else if (e.ctrlKey && e.keyCode == 76) {
            exchangeLanguageDirection();
        }

        if ($("#inputArea").val() == "") {
            $("#yandexOutputArea").val("");
            $("#bingOutputArea").val("");
            $("#mymemoryOutputArea").val("");
            resizeOutputTextareas();
        }
    });

    $(".translateContainer").keyup(function(e) {
        if (e.ctrlKey && e.keyCode == 13) {
            translateText();
        }
    });

    $("#translateDirectionTo, #translateDirectionFrom").change(function() {
        saveTranslateDirectionToStorage();
    });

    $("#inputArea").change(function() {
        localStorage.inputArea = $("#inputArea").val();
    });

    $("#settingsMenuItem").click(function() {
        showContainer("settingsPopupContainer");
    });

    $("#loginMenuItem").click(function() {
        showContainer("loginPopupContainer");
    });

    $(".backIcon").click(function() {
        showContainer("mainContainer");
    });

    $(".enableEngine_js").change(function(e) {
        if ($(".enableEngine_js:checked").length == 0) {
            this.checked = !this.checked;
        }

        var enginesVisibility = {
            yandexOutputContainer: true,
            bingOutputContainer: true,
            mymemoryOutputContainer: true
        };

        $(".enableEngine_js").each(function() {
            if (!this.checked) {
                switch (this.id) {
                    case "enableYandex":
                        enginesVisibility.yandexOutputContainer = false;
                        break;
                    case "enableBing":
                        enginesVisibility.bingOutputContainer = false;
                        break;
                    case "enableMymemory":
                        enginesVisibility.mymemoryOutputContainer= false;
                        break;
                }
            }
        });

        settingsManager.setTranslateEnginesVisibility(enginesVisibility);

    });
}

function setSpinnerForYandexOn() {
    $("#yandexImg").attr("src", "../images/spinner.gif");
}

function setSpinnerForYandexOff() {
    $("#yandexImg").attr("src", "../images/yandex/yandex_icon_48x48.png");
}

function setSpinnerForBingOn() {
    $("#bingImg").attr("src", "../images/spinner.gif");
}

function setSpinnerForBingOff() {
    $("#bingImg").attr("src", "../images/bing/bing_icon_48x48.png");
}


function setSpinnerForMymemoryOn() {
    $("#mymemoryImg").attr("src", "../images/spinner.gif");
}

function setSpinnerForMymemoryOff() {
    $("#mymemoryImg").attr("src", "../images/mymemory/mymemory_icon_48x48.png");
}

function resizeYandexTextarea () {
    $('#yandexOutputArea').trigger('autosize.resize');
}

function resizeBingTextarea () {
    $('#bingOutputArea').trigger('autosize.resize');
}

function resizeMymemoryTextarea () {
    $('#mymemoryOutputArea').trigger('autosize.resize');
}

function resizeOutputTextareas() {
    $('#yandexOutputArea, #bingOutputArea, #mymemoryOutputArea').trigger('autosize.resize');
}

function showContainer(className) {
    $(".content").children().css("display", "none");
    $("." + className).show();
}
