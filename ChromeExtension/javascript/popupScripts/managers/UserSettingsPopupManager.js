function UserSettingsPopupManager() {
    var it = this;
    var languagesList = {
        "ar": "Arabic",
        "bg": "Bulgarian",
        "ca": "Catalan",
        "cs": "Czech",
        "da": "Danish",
        "de": "German",
        "el": "Greek",
        "es": "Spanish",
        "nl": "Dutch",
        "en": "English",
        "et": "Estonian",
        "fi": "Finnish",
        "fr": "French",
        "he": "Hebrew",
        "hu": "Hungarian",
        "id": "Indonesian",
        "it": "Italian",
        "lt": "Lithuanian",
        "lv": "Latvian",
        "ms": "Malay",
        "no": "Norwegian",
        "pl": "Polish",
        "pt": "Portuguese",
        "ro": "Romanian",
        "ru": "Russian",
        "sk": "Slovak",
        "sl": "Slovenian",
        "sv": "Swedish",
        "th": "Thai",
        "uk": "Ukrainian",
        "vi": "Vietnamese"
    };

    this.applyUserSettings = function() {
        it.updateTranslateEnginesVisibility();
        it.prepareTranslateDirections();
        it.updateDefaultTranslateDirection();
    };

    this.updateTranslateEnginesVisibility = function() {
        if (localStorage.enginesVisibility) {
            var enginesVisibility =  JSON.parse(localStorage.enginesVisibility);

            for (var key in enginesVisibility) {
                if (enginesVisibility.hasOwnProperty(key)) {
                    if (!!enginesVisibility[key]) {
                        $("#" + key).show();
                    } else {
                        $("#" + key).hide();
                    }
                }
            }
        }
    };

    /**
     * @param data should be in the next format: {yandexOutputContainer: true, bingOutputContainer: false, ...},
     * where first parameter is the container id and the value is a display option.
     */
    this.setTranslateEnginesVisibility = function(data) {
        localStorage.enginesVisibility = JSON.stringify(data);
        it.updateTranslateEnginesVisibility();
    };

    this.prepareTranslateDirections = function() {
        $.each(languagesList, function(key, value) {
            $('#translateDirectionFrom')
                .append($("<option></option>")
                    .attr("value",key)
                    .text(value));
            $('#translateDirectionTo')
                .append($("<option></option>")
                    .attr("value",key)
                    .text(value));
        });
    };

    this.updateDefaultTranslateDirection = function() {
        if (!localStorage.languageFrom) {
            localStorage.languageFrom = "en";
        }

        if (!localStorage.languageTo) {
            localStorage.languageTo = "uk";
        }

        $("#translateDirectionFrom option[value=" + localStorage.languageFrom + "]").attr("selected", "true");
        $("#translateDirectionTo option[value=" + localStorage.languageTo +"]").attr("selected", "true");

    };
}
