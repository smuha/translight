function UIPopupManager() {
    var it = this;
    this.buildInterface = function() {
        it.setPopupWidth();
        it.buildSettingsInterface();
    };

    this.setPopupWidth = function() {
        var screenWidth = window.screen.availWidth;
        if (screenWidth >= 1920) {
            $(".mainContainer").css({width: 450});
            $(".outputTextArea").css({width: 409});
        } else if ((screenWidth >= 1600) && (screenWidth < 1920)) {
            $(".mainContainer").css({width: 440});
            $(".outputTextArea").css({width: 399});
        } else if ((screenWidth >= 1366) && (screenWidth < 1600)) {
            $(".mainContainer").css({width: 400});
            $(".outputTextArea").css({width: 359});
        } else if (screenWidth <= 1366) {
            $(".mainContainer").css({width: 390});
            $(".outputTextArea").css({width: 349});
        }
    };

    this.buildSettingsInterface = function() {
        it.setEnabledEnginesCheckboxes();
    };

    this.setEnabledEnginesCheckboxes = function() {
        if (localStorage.enginesVisibility) {
            var enginesVisibility = JSON.parse(localStorage.enginesVisibility);
            for (var key in enginesVisibility) {
                if (enginesVisibility.hasOwnProperty(key) && !enginesVisibility[key]) {
                    switch (key) {
                        case "yandexOutputContainer":
                            $('#enableYandex').attr('checked', false);
                            break;
                        case "bingOutputContainer":
                            $('#enableBing').attr('checked', false);
                            break;
                        case "mymemoryOutputContainer":
                            $('#enableMymemory').attr('checked', false);
                            break;
                    }
                }
            }
        }
    };
}